import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import DemoHookPage from "./Pages/DemoHookPage/DemoHookPage";
import Header from "./Components/Header/Header";

function App() {
  return (
    <div className="text-center">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/detail/:maPhim" component={DetailPage} />
          <Route
            path="/hook"
            render={() => {
              return <DemoHookPage />;
            }}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
