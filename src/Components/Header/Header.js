import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
      <div className=" p-5 bg-white shadow flex justify-start space-x-5 ">
        <NavLink
          to="/"
          exact
          className=" bg-blue-400 text-white rounded px-5 py-2 shadow-lg  "
          activeClassName="bg-red-600"
        >
          Home page
        </NavLink>

        <NavLink
          // activeStyle={{
          //   color: "red",
          // }}
          to="/hook"
          className=" bg-blue-400 text-white rounded px-5 py-2 shadow-lg  "
          activeClassName="bg-red-600"
        >
          Demo Hook
        </NavLink>
      </div>
    );
  }
}
