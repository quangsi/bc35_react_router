import React, { Component } from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default class MovieItem extends Component {
  render() {
    let { movie } = this.props;
    return (
      <Card
        hoverable
        style={{
          width: "100%",
        }}
        cover={<img alt="example" src={movie.hinhAnh} />}
      >
        <Meta
          title={movie.tenPhim}
          description={
            movie.moTa.length < 60
              ? movie.moTa
              : movie.moTa.slice(0, 60) + "..."
          }
        />
        <NavLink to={`/detail/${movie.maPhim}`}>
          <button className="bg-red-500 text-white rounded w-full py-3 ">
            Xem chi tiết
          </button>
        </NavLink>
      </Card>
    );
  }
}
