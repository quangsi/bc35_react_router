import axios from "axios";
import React, { Component } from "react";
import MovieItem from "./MovieItem";

export default class HomePage extends Component {
  state = {
    movieArr: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjAzLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTc1MDQwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg1ODk4MDAwfQ.KXn1XtehbphvfW3OSUFlLIzSrEtSLDtDQG4BgF38Cus",
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ movieArr: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderMovieList = () => {
    return this.state.movieArr.map((item) => {
      return <MovieItem movie={item} />;
    });
  };
  render() {
    return (
      <div className="container mx-auto">
        <div className="grid grid-cols-6 gap-4">{this.renderMovieList()}</div>
      </div>
    );
  }
}
/**
 * {
    "maPhim": 10499,
    "tenPhim": "Cat and Dog 2",
    "biDanh": "cat-and-dog-2",
    "trailer": "Trailer",
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/cat-and-dog-2_gp01.jpg",
    "moTa": "Đã lâu k gặp, nay tình cờ mèo gặp 2 bạn chó già đi thể dục thế là rủ nhau đi uống cafe",
    "maNhom": "GP01",
    "ngayKhoiChieu": "2022-10-17T13:24:53.647",
    "danhGia": 6,
    "hot": true,
    "dangChieu": true,
    "sapChieu": true
}
 */
