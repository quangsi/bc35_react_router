import axios from "axios";
import React, { Component } from "react";
import Header from "../../Components/Header/Header";

export default class DetailPage extends Component {
  state = {
    detail: null,
  };
  componentDidMount() {
    console.log(this.props);
    let { maPhim } = this.props.match.params;
    let config = {
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjAzLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTc1MDQwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg1ODk4MDAwfQ.KXn1XtehbphvfW3OSUFlLIzSrEtSLDtDQG4BgF38Cus",
      },
    };

    axios
      .get(
        `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`,
        config
      )
      .then((res) => {
        // setState sau khi lấy được data từ api.
        // setState=> render chạy lại => layout được update
        this.setState({
          detail: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    return (
      <div className="space-y-10">
        <p>{this.props.match.params.maPhim}</p>

        <img
          className="w-1/3  h-96 object-cover"
          src={this.state.detail?.hinhAnh}
          alt=""
        />
      </div>
    );
  }
}
