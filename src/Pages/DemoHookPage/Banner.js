import React, { memo, useEffect } from "react";
function Banner() {
  useEffect(() => {
    return () => {
      console.log("Banner will unmount");
    };
    // will unMount
  }, []);

  console.log("Banner re-render");
  return <div className="p-20 bg-black text-white">Banner</div>;
}

export default memo(Banner);
