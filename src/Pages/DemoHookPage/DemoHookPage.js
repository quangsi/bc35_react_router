import React, { useEffect, useState } from "react";
import Banner from "./Banner";

export default function DemoHookPage() {
  let [like, setLike] = useState(1);
  let [share, setShare] = useState(1);

  useEffect(() => {
    console.log("Did mount");
  }, [like, share]);
  /**
   * useEffect() : didMount, didUpdate, willUnmount
   * luôn luôn đi kèm dependencyList ( array )
   */
  let handlePlushLike = () => {
    setLike(like + 1);
  };
  let handlePlusShare = () => {
    setShare(share + 1);
  };
  console.log("render");
  return (
    <div className="text-center">
      <h2>DemoHookPage</h2>
      <p className="font-bold text-lg">Like: {like}</p>
      <button
        onClick={handlePlushLike}
        className="px-5 py-2 rounded bg-yellow-300"
      >
        Plus like
      </button>

      <p className="font-bold text-lg">Share: {share}</p>

      <button
        onClick={handlePlusShare}
        className="px-5 py-2 rounded bg-purple-600"
      >
        Plus share
      </button>

      {like < 10 && <Banner />}
    </div>
  );
}
// rfc
